# Phone book project with Spring boot
This project has two main feature including create and search contacts.

## Create contact
Contacts needs to have user's Github repositories names. In order to avoid affecting throughput, 
after creating contact, an async job triggers to get user repositories from Github.
Also there is an background job that triggers every 5 minutes to fetch github repositories if previous try is failed.
```http request
PUT /contacts HTTP/1.1
Host: 127.0.0.1:8080
Content-Type: application/json
{
	"name":"Burr Sutter",
	"phoneNumber":"+989112233445",
	"email":"burrsutter@gmail.com",
	"organization":"Red Hat",
	"github":"burrsutter"
}
```

## Search contacts
All fields that uses in search is optional.

Result will be include contacts that all requested fields exactly matches.
```http request
POST /contacts/search HTTP/1.1
Host: 127.0.0.1:8080
Content-Type: application/json

{
	"name":"Burr Sutter",
	"phoneNumber":"+989112233445",
	"email":"burrsutter@gmail.com",
	"organization":"Red Hat",
	"github":"burrsutter"
}
```

## Search contacts with details
This endpoint returns more detail about contacts.
```http request
POST /contacts/detail/search HTTP/1.1
Host: 127.0.0.1:8080
Content-Type: application/json

{
	"name":"Burr Sutter",
	"phoneNumber":"+989112233445",
	"email":"burrsutter@gmail.com",
	"organization":"Red Hat",
	"github":"burrsutter"
}
```
# Test
Integeration test runs with `testcontainers` library that runs docker for required resources. 
`mherwig/alpine-java-mongo` will run and test uses its mongo to test in real world.


# Run
There are two way of running this application.

1- To run application with docker you just need to execute `./run.sh` script in root directory.
Base image of deployment is `mherwig/alpine-java-mongo` that has java and mongo installed in it.
Every time you execute `./run.sh`, previous container will delete and all data will purge.

2- you just need to put your mongo connection info into `application.yml` and run with maven command `mvn clean package`

In both way application will listen on port 8080 of your system.
