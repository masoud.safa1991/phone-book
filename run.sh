#!/bin/sh

imageName="snappbox/phonebook"
containerName="phonebook"

#remove previous container and image if exists
docker stop $containerName
docker rm $containerName
docker rmi $imageName;

mvn clean package
docker build -t $imageName .

docker run -d --name=$containerName -p=8080:8080 $imageName
docker logs -f $containerName