package com.snappbox.phonebook.test;

import com.snappbox.phonebook.services.models.ContactCreationModel;
import com.snappbox.phonebook.services.models.ContactSearchModel;

public class MockModelGenerator {
    public static ContactCreationModel creationModel() {
        ContactCreationModel model = new ContactCreationModel();
        model.setEmail("burrsutter@gmail.com");
        model.setName("Burr Sutter");
        model.setGithub("burrsutter");
        model.setOrganization("Red Hat");
        model.setPhoneNumber("+989112233445");
        return model;
    }

    public static ContactSearchModel searchModel() {
        ContactSearchModel model = new ContactSearchModel();
        model.setEmail("burrsutter@gmail.com");
        model.setName("Burr Sutter");
        model.setGithub("burrsutter");
        model.setOrganization("Red Hat");
        model.setPhoneNumber("+989112233445");
        return model;
    }
}