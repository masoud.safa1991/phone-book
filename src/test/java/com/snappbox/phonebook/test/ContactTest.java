package com.snappbox.phonebook.test;

import com.snappbox.phonebook.repositories.ContactRepository;
import com.snappbox.phonebook.services.models.ContactCreationModel;
import com.snappbox.phonebook.services.models.ContactModel;
import com.snappbox.phonebook.services.models.ContactSearchModel;
import lombok.extern.slf4j.Slf4j;
import org.junit.Assert;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.util.TestPropertyValues;
import org.springframework.context.ApplicationContextInitializer;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.testcontainers.shaded.com.fasterxml.jackson.databind.ObjectMapper;

import java.util.List;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SpringBootTest
@AutoConfigureMockMvc
@ContextConfiguration(initializers = ContactTest.MongoDbInitializer.class)
@Slf4j
public class ContactTest {

    private ObjectMapper mapper = new ObjectMapper();
    @Autowired
    private MockMvc mockMvc;

    @Autowired
    private ContactRepository repository;

    private static MongoDbContainer mongoDbContainer;

    @BeforeAll
    public static void startContainerAndPublicPortIsAvailable() {
        mongoDbContainer = new MongoDbContainer();
        mongoDbContainer.start();
    }


    @Test
    public void testCreateAndSearchContacts() throws Exception {
        repository.deleteAll();

        ContactCreationModel creationModel = MockModelGenerator.creationModel();
        ContactSearchModel searchModel = MockModelGenerator.searchModel();

        createContact(creationModel);
        Assert.assertEquals(1, repository.findAll().size());
        searchContacts(searchModel, 1);


        createContact(creationModel);
        createContact(creationModel);
        Assert.assertEquals(3, repository.findAll().size());
        searchContacts(searchModel, 3);


    }

    private void createContact(ContactCreationModel creationModel) throws Exception {
        mockMvc.perform(
                put("/contacts")
                        .contentType("application/json")
                        .content(mapper.writeValueAsString(creationModel)))
                .andExpect(status().isOk());
    }

    private void searchContacts(ContactSearchModel searchModel, int expectedResults) throws Exception {
        mockMvc.perform(
                post("/contacts/search")
                        .contentType("application/json")
                        .content(mapper.writeValueAsString(searchModel)))
                .andExpect(status().isOk())
                .andExpect(result -> {
                            List<ContactModel> response = mapper.readValue(
                                    result.getResponse().getContentAsString(),
                                    mapper.getTypeFactory().constructCollectionType(List.class, ContactModel.class)
                            );
                            Assert.assertEquals(response.size(), expectedResults);

                            for (ContactModel model : response) {
                                checkSearchFields(searchModel.getEmail(), model.getEmail());
                                checkSearchFields(searchModel.getGithub(), model.getGithubUserName());
                                checkSearchFields(searchModel.getName(), model.getName());
                                checkSearchFields(searchModel.getOrganization(), model.getOrganization());
                                checkSearchFields(searchModel.getPhoneNumber(), model.getPhoneNumber());
                            }

                        }
                );
    }

    private void checkSearchFields(String request, String response) {
        if (request != null && request.trim().length() > 0)
            Assert.assertEquals(request, response);
    }

    public static class MongoDbInitializer implements ApplicationContextInitializer<ConfigurableApplicationContext> {
        @Override
        public void initialize(ConfigurableApplicationContext configurableApplicationContext) {
            log.info("Overriding Spring Properties for mongodb !!!!!!!!!");

            TestPropertyValues values = TestPropertyValues.of(
                    "spring.data.mongodb.host=" + mongoDbContainer.getContainerIpAddress(),
                    "spring.data.mongodb.port=" + mongoDbContainer.getPort()
            );
            values.applyTo(configurableApplicationContext);
        }
    }
}