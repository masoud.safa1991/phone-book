package com.snappbox.phonebook.controllers;

import com.snappbox.phonebook.services.ContactService;
import com.snappbox.phonebook.services.models.ContactCreationModel;
import com.snappbox.phonebook.services.models.ContactDetailModel;
import com.snappbox.phonebook.services.models.ContactModel;
import com.snappbox.phonebook.services.models.ContactSearchModel;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping("/contacts")
public class ContactController {
    private final ContactService service;

    @Autowired
    public ContactController(ContactService service) {
        this.service = service;
    }


    @PutMapping("")
    public ContactModel createContact(@Valid @RequestBody ContactCreationModel model) {
        return service.createContact(model);
    }

    @PostMapping("/search")
    public List<ContactModel> search(@RequestBody ContactSearchModel model) {
        return service.search(model);
    }

    @PostMapping("/detail/search")
    public List<ContactDetailModel> searchWithDetail(@RequestBody ContactSearchModel model) {
        return service.searchWithDetail(model);
    }

}
