package com.snappbox.phonebook.clients;

import com.snappbox.phonebook.clients.models.GitHubRepositoryModel;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.List;

@FeignClient(name = "github", url = "https://api.github.com")
public interface GitHubClient {

    @GetMapping("/users/{username}/repos?per_page=100")
    List<GitHubRepositoryModel> getRepositories(
            @PathVariable("username") String username,
            @RequestParam("page") int page
    );
}
