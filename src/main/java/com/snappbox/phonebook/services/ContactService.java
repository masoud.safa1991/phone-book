package com.snappbox.phonebook.services;


import com.snappbox.phonebook.entities.Contact;
import com.snappbox.phonebook.jobs.FetchGitHubDataJob;
import com.snappbox.phonebook.repositories.ContactRepository;
import com.snappbox.phonebook.services.models.ContactCreationModel;
import com.snappbox.phonebook.services.models.ContactDetailModel;
import com.snappbox.phonebook.services.models.ContactModel;
import com.snappbox.phonebook.services.models.ContactSearchModel;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.convert.ConversionService;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
public class ContactService {
    private final ContactRepository repository;
    private final ConversionService conversionService;
    private final FetchGitHubDataJob fetchGitHubDataJob;

    @Autowired
    public ContactService(
            ContactRepository repository,
            ConversionService conversionService,
            FetchGitHubDataJob fetchGitHubDataJob
    ) {
        this.repository = repository;
        this.conversionService = conversionService;
        this.fetchGitHubDataJob = fetchGitHubDataJob;
    }


    public ContactModel createContact(ContactCreationModel model) {
        Contact entity = conversionService.convert(model, Contact.class);
        entity = repository.save(entity);
        fetchGitHubDataJob.fetchGitHubData(entity);//async job
        return conversionService.convert(entity, ContactModel.class);
    }

    public List<ContactModel> search(ContactSearchModel model) {
        return repository.search(model)
                .stream()
                .map(entity -> conversionService.convert(entity, ContactModel.class))
                .collect(Collectors.toList());
    }

    public List<ContactDetailModel> searchWithDetail(ContactSearchModel model) {
        return repository.search(model)
                .stream()
                .map(entity -> conversionService.convert(entity, ContactDetailModel.class))
                .collect(Collectors.toList());
    }


    public List<Contact> getUnprocessedGitHubData() {
        return repository.findAllByRepositoriesIsNull();
    }

    public void setRepositories(String contactId, List<String> repositories) {
        Optional<Contact> optionalContact = repository.findById(contactId);
        if (optionalContact.isPresent()) {
            Contact contact = optionalContact.get();
            contact.setRepositories(repositories);
            repository.save(contact);
        }
    }

}
