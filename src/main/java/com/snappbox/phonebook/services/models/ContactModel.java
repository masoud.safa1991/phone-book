package com.snappbox.phonebook.services.models;

import lombok.Data;

@Data
public class ContactModel {
    String name;
    String phoneNumber;
    String email;
    String organization;
    String githubUserName;
}
