package com.snappbox.phonebook.services.models;

import lombok.Data;

import java.util.Date;
import java.util.List;

@Data
public class ContactDetailModel {
    String id;
    String name;
    String phoneNumber;
    String email;
    String organization;
    String githubUserName;
    List<String> repositories;
    Date creationDate;
}
