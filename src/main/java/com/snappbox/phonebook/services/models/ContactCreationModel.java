package com.snappbox.phonebook.services.models;

import lombok.Data;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;

@Data
public class ContactCreationModel {
    @NotNull(message = "error.validation.not_nullable")
    String name;
    @NotNull(message = "error.validation.not_nullable")
    @Pattern(regexp = "(\\+98|0)[0-9]{10}", message = "error.validation.phone_number")
    String phoneNumber;
    @NotNull(message = "error.validation.not_nullable")
    @Email(message = "error.validation.email")
    String email;
    @NotNull(message = "error.validation.not_nullable")
    String organization;
    @NotNull(message = "error.validation.not_nullable")
    String github;
}
