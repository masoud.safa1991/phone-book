package com.snappbox.phonebook.services.models;

import lombok.Data;

@Data
public class ContactSearchModel {
    String name;
    String phoneNumber;
    String email;
    String organization;
    String github;
}
