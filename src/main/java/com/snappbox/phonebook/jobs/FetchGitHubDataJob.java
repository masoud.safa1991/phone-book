package com.snappbox.phonebook.jobs;

import com.snappbox.phonebook.clients.GitHubClient;
import com.snappbox.phonebook.clients.models.GitHubRepositoryModel;
import com.snappbox.phonebook.entities.Contact;
import com.snappbox.phonebook.services.ContactService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Lazy;
import org.springframework.scheduling.annotation.Async;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@Component
public class FetchGitHubDataJob {

    private final GitHubClient client;
    private final ContactService service;

    @Autowired
    public FetchGitHubDataJob(GitHubClient client, @Lazy ContactService service) {
        this.client = client;
        this.service = service;
    }


    @Scheduled(fixedDelay = 300_000)
    public void fetchUnprocessedData() {
        List<Contact> contacts = service.getUnprocessedGitHubData();
        contacts.forEach(this::fetchGitHubData);
    }

    @Async
    public void fetchGitHubData(Contact contact) {
        ArrayList<GitHubRepositoryModel> repositories = new ArrayList<>();
        int page = 1;
        while (true) {
            List<GitHubRepositoryModel> tempRepos = client.getRepositories(contact.getGithubUserName(), page);
            if (tempRepos.isEmpty())
                break;
            page++;
            repositories.addAll(tempRepos);
        }
        service.setRepositories(
                contact.getId(),
                repositories
                        .stream()
                        .map(GitHubRepositoryModel::getName)
                        .distinct()
                        .collect(Collectors.toList())
        );
    }

}
