package com.snappbox.phonebook.error.models;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.AllArgsConstructor;
import lombok.Data;
import org.springframework.http.HttpStatus;

import java.util.List;

@Data
@AllArgsConstructor
public class ErrorDto {
    List<CodedMessage> errors;

    @JsonIgnore
    HttpStatus httpStatus;
}
