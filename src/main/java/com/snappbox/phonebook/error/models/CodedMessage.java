package com.snappbox.phonebook.error.models;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class CodedMessage {
    String code;
    String message;
}
