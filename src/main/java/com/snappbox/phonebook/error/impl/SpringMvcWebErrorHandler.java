package com.snappbox.phonebook.error.impl;

import com.snappbox.phonebook.error.WebErrorHandler;
import org.springframework.core.Ordered;
import org.springframework.core.annotation.Order;
import org.springframework.http.HttpStatus;
import org.springframework.http.converter.HttpMessageNotReadableException;
import org.springframework.stereotype.Component;
import org.springframework.util.MimeType;
import org.springframework.web.HttpMediaTypeNotAcceptableException;
import org.springframework.web.HttpMediaTypeNotSupportedException;
import org.springframework.web.HttpRequestMethodNotSupportedException;
import org.springframework.web.bind.MissingServletRequestParameterException;
import org.springframework.web.servlet.NoHandlerFoundException;

import javax.servlet.ServletException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

@Component
@Order(Ordered.HIGHEST_PRECEDENCE + 200)
public class SpringMvcWebErrorHandler implements WebErrorHandler {
    @Override
    public boolean canHandle(Throwable exception) {
        return exception instanceof ServletException || exception instanceof HttpMessageNotReadableException;
    }

    @Override
    public List<String> handle(Throwable exception) {
        String error = UNKNOWN_ERROR;
        if (exception instanceof HttpRequestMethodNotSupportedException) error = "error.web.method_not_supported";
        else if (exception instanceof HttpMediaTypeNotAcceptableException) error = "error.web.not_acceptable";
        else if (exception instanceof HttpMediaTypeNotSupportedException) error = "error.web.unsupported_media_type";
        else if (exception instanceof NoHandlerFoundException) error = "error.web.not_found";
        else if (exception instanceof HttpMessageNotReadableException) error = "error.web.invalid_body";
        else if (exception instanceof MissingServletRequestParameterException) error = "error.web.missing_parameters";
        return Collections.singletonList(error);

    }

    @Override
    public HttpStatus statusCode(Throwable exception) {
        if (exception instanceof HttpRequestMethodNotSupportedException) return HttpStatus.METHOD_NOT_ALLOWED;
        if (exception instanceof HttpMediaTypeNotAcceptableException) return HttpStatus.NOT_ACCEPTABLE;
        if (exception instanceof HttpMediaTypeNotSupportedException) return HttpStatus.UNSUPPORTED_MEDIA_TYPE;
        if (exception instanceof NoHandlerFoundException) return HttpStatus.NOT_FOUND;
        if (exception instanceof HttpMessageNotReadableException) return HttpStatus.BAD_REQUEST;
        if (exception instanceof MissingServletRequestParameterException) return HttpStatus.BAD_REQUEST;
        else return HttpStatus.INTERNAL_SERVER_ERROR;
    }

    @Override
    public List<Object> getArguments(Throwable exception) {
        if (exception instanceof HttpRequestMethodNotSupportedException)
            return Arrays.asList(((HttpRequestMethodNotSupportedException) exception).getMethod());
        if (exception instanceof HttpMediaTypeNotAcceptableException)
            return (((HttpMediaTypeNotAcceptableException) exception).getSupportedMediaTypes().stream().map(MimeType::toString).collect(Collectors.toList()));
        if (exception instanceof NoHandlerFoundException)
            return Arrays.asList(((NoHandlerFoundException) exception).getRequestURL());
        if (exception instanceof MissingServletRequestParameterException)
            return Arrays.asList(((MissingServletRequestParameterException) exception).getParameterName());
        else return new ArrayList<>();
    }
}
