package com.snappbox.phonebook.error.impl;

import com.snappbox.phonebook.error.WebErrorHandler;
import org.springframework.core.Ordered;
import org.springframework.core.annotation.Order;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

@Component
@Order(Ordered.LOWEST_PRECEDENCE)
public class LastResortWebErrorHandler implements WebErrorHandler {


    @Override
    public boolean canHandle(Throwable exception) {
        return true;
    }

    @Override
    public List<String> handle(Throwable exception) {
        return Collections.singletonList(UNKNOWN_ERROR);
    }

    @Override
    public HttpStatus statusCode(Throwable exception) {
        return HttpStatus.INTERNAL_SERVER_ERROR;
    }

    @Override
    public List<Object> getArguments(Throwable exception) {
        return new ArrayList<>();
    }
}
