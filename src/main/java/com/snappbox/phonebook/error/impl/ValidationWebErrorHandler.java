package com.snappbox.phonebook.error.impl;

import com.snappbox.phonebook.error.WebErrorHandler;
import org.springframework.context.support.DefaultMessageSourceResolvable;
import org.springframework.core.Ordered;
import org.springframework.core.annotation.Order;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Component;
import org.springframework.validation.FieldError;
import org.springframework.validation.ObjectError;
import org.springframework.web.bind.MethodArgumentNotValidException;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@Component
@Order(Ordered.HIGHEST_PRECEDENCE)
public class ValidationWebErrorHandler implements WebErrorHandler {
    @Override
    public boolean canHandle(Throwable exception) {
        return exception instanceof MethodArgumentNotValidException;
    }

    @Override
    public List<String> handle(Throwable exception) {
        return ((MethodArgumentNotValidException) exception)
                .getBindingResult()
                .getAllErrors()
                .stream()
                .map(DefaultMessageSourceResolvable::getDefaultMessage)
                .collect(Collectors.toList());

    }

    @Override
    public HttpStatus statusCode(Throwable exception) {
        return HttpStatus.BAD_REQUEST;
    }

    @Override
    public List<Object> getArguments(Throwable exception) {
        MethodArgumentNotValidException invalidException = (MethodArgumentNotValidException) exception;
        List<ObjectError> errors = (invalidException.getBindingResult()).getAllErrors();
        ArrayList<Object> args = new ArrayList<>();
        errors.forEach(error -> {
            if (error instanceof FieldError)
                args.add(((FieldError) error).getField());
        });
        return args;
    }
}
