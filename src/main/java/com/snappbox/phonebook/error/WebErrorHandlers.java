package com.snappbox.phonebook.error;

import com.snappbox.phonebook.error.models.CodedMessage;
import com.snappbox.phonebook.error.models.ErrorDto;
import org.springframework.beans.factory.ObjectProvider;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.core.annotation.AnnotationAwareOrderComparator;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.Locale;
import java.util.stream.Collectors;

@Component
public class WebErrorHandlers {
    private final MessageSource messageSource;
    private List<WebErrorHandler> errorHandlers;

    @Autowired
    public WebErrorHandlers(ObjectProvider<List<WebErrorHandler>> handlerProvider, MessageSource messageSource) {
        this.messageSource = messageSource;

        errorHandlers = handlerProvider.getIfAvailable();
        errorHandlers.sort(AnnotationAwareOrderComparator.INSTANCE);
    }

    public ErrorDto handleError(Throwable exception, Locale locale) {
        WebErrorHandler handler = firstHandler(exception);
        List<String> errorCodes = handler.handle(exception);

        return translateMessages(handler, errorCodes, exception, locale);
    }

    private ErrorDto translateMessages(WebErrorHandler handler,
                                       List<String> errorCodes,
                                       Throwable exception,
                                       Locale locale) {
        return new ErrorDto(
                errorCodes
                        .stream()
                        .map(it -> messageFor(it, handler.getArguments(exception), locale))
                        .collect(Collectors.toList()),
                handler.statusCode(exception)
        );
    }

    private CodedMessage messageFor(String code, List<Object> args, Locale locale) {
        String message = messageSource.getMessage(code, args.toArray(), "N/A", locale);
        return new CodedMessage(code, message);
    }

    private WebErrorHandler firstHandler(Throwable exception) {
        for (WebErrorHandler handler : errorHandlers)
            if (handler.canHandle(exception))
                return handler;
        return null;
    }
}
