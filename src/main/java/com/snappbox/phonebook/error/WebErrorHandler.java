package com.snappbox.phonebook.error;

import org.springframework.http.HttpStatus;

import java.util.List;

public interface WebErrorHandler {
    String UNKNOWN_ERROR = "error.unknown";

    boolean canHandle(Throwable exception);

    List<String> handle(Throwable exception);

    HttpStatus statusCode(Throwable exception);

    List<Object> getArguments(Throwable exception);

}
