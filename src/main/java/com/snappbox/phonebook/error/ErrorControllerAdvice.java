package com.snappbox.phonebook.error;

import com.snappbox.phonebook.error.models.ErrorDto;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;

import java.util.Locale;

@RestControllerAdvice
public class ErrorControllerAdvice {
    private final WebErrorHandlers errorHandlers;

    public ErrorControllerAdvice(WebErrorHandlers errorHandlers) {
        this.errorHandlers = errorHandlers;
    }

    @ExceptionHandler(Throwable.class)
    public ResponseEntity<ErrorDto> handleError(Throwable throwable, Locale locale) {
        ErrorDto error = errorHandlers.handleError(throwable, locale);
        return new ResponseEntity(error, error.getHttpStatus());
    }
}
