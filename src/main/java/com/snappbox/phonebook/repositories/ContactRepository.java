package com.snappbox.phonebook.repositories;

import com.snappbox.phonebook.entities.Contact;
import org.springframework.data.mongodb.repository.MongoRepository;

import java.util.List;

public interface ContactRepository extends MongoRepository<Contact, String>, ContactRepositoryExtra {

    List<Contact> findAllByRepositoriesIsNull();
}
