package com.snappbox.phonebook.repositories;

import com.snappbox.phonebook.entities.Contact;
import com.snappbox.phonebook.services.models.ContactSearchModel;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;

import java.util.List;

public class ContactRepositoryImpl implements ContactRepositoryExtra {

    private final MongoTemplate mongoTemplate;

    @Autowired
    public ContactRepositoryImpl(MongoTemplate mongoTemplate) {
        this.mongoTemplate = mongoTemplate;
    }


    @Override
    public List<Contact> search(ContactSearchModel model) {
        Query query = new Query();
        addCriteria(query, "name", model.getName());
        addCriteria(query, "email", model.getEmail());
        addCriteria(query, "phoneNumber", model.getPhoneNumber());
        addCriteria(query, "organization", model.getOrganization());
        addCriteria(query, "githubUserName", model.getGithub());

        query.with(Sort.by(Sort.Direction.DESC, "creationDate"));
        return mongoTemplate.find(query, Contact.class);
    }

    private void addCriteria(Query query, String field, String value) {
        if (value != null && value.trim().length() > 0)
            query.addCriteria(Criteria.where(field).is(value));

    }
}
