package com.snappbox.phonebook.repositories;

import com.snappbox.phonebook.entities.Contact;
import com.snappbox.phonebook.services.models.ContactSearchModel;

import java.util.List;

public interface ContactRepositoryExtra {

    List<Contact> search(ContactSearchModel model);
}
