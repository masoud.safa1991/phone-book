package com.snappbox.phonebook.converters;

import com.snappbox.phonebook.entities.Contact;
import com.snappbox.phonebook.services.models.ContactCreationModel;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

import java.util.Date;

@Component
public class ContactCreationModelToEntityConverter implements Converter<ContactCreationModel, Contact> {
    @Override
    public Contact convert(ContactCreationModel source) {
        Contact entity = new Contact();
        entity.setName(source.getName());
        entity.setEmail(source.getEmail());
        entity.setGithubUserName(source.getGithub());
        entity.setPhoneNumber(source.getPhoneNumber());
        entity.setOrganization(source.getOrganization());
        entity.setCreationDate(new Date());
        return entity;
    }
}
