package com.snappbox.phonebook.converters;

import com.snappbox.phonebook.entities.Contact;
import com.snappbox.phonebook.services.models.ContactDetailModel;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

@Component
public class ContactEntityToDetailModelConverter implements Converter<Contact, ContactDetailModel> {
    @Override
    public ContactDetailModel convert(Contact source) {
        ContactDetailModel model = new ContactDetailModel();
        model.setId(source.getId());
        model.setName(source.getName());
        model.setEmail(source.getEmail());
        model.setGithubUserName(source.getGithubUserName());
        model.setPhoneNumber(source.getPhoneNumber());
        model.setOrganization(source.getOrganization());
        model.setRepositories(source.getRepositories());
        model.setCreationDate(source.getCreationDate());
        return model;
    }
}
