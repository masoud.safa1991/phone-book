package com.snappbox.phonebook.converters;

import com.snappbox.phonebook.entities.Contact;
import com.snappbox.phonebook.services.models.ContactModel;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

@Component
public class ContactEntityToModelConverter implements Converter<Contact, ContactModel> {
    @Override
    public ContactModel convert(Contact source) {
        ContactModel model = new ContactModel();
        model.setName(source.getName());
        model.setEmail(source.getEmail());
        model.setGithubUserName(source.getGithubUserName());
        model.setPhoneNumber(source.getPhoneNumber());
        model.setOrganization(source.getOrganization());
        return model;
    }
}
