package com.snappbox.phonebook.entities;

import lombok.Data;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import java.util.Date;
import java.util.List;


@Data
@Document("contacts")
public class Contact {
    @Id
    String id;
    String name;
    String phoneNumber;
    String email;
    String organization;
    String githubUserName;
    List<String> repositories;
    Date creationDate;

}
