FROM mherwig/alpine-java-mongo


COPY entrypoint.sh /data/entrypoint.sh

ADD target/phonebook-0.0.1-SNAPSHOT.jar /data/phonebook.jar

CMD /data/entrypoint.sh
EXPOSE 8080